Bootstrap: docker
From: ubuntu

%setup
     ## The "%setup"-part of this script is called to bootstrap an empty
     ## container. It copies the source files from the branch of your
     ## repository where this file is located into the container to the
     ## directory "/planner". Do not change this part unless you know
     ## what you are doing and you are certain that you have to do so.

    REPO_ROOT=`dirname $SINGULARITY_BUILDDEF`
    cp -r $REPO_ROOT/ $SINGULARITY_ROOTFS/planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.

    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install cmake g++ gcc make bison flex zlib1g-dev

    ## Build your planner
    cd /planner
    # build madagascar
    make

%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    /planner/plan.sh $1 $2 $3

## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name        freelunch-madagascar
Description Incremental Satisfiasfiability based Planning using Madagascar by Jussi Rintanen for encoding.
Authors     Tomas Balyo <biotomas@gmail.com> and Stephan Gocht <gocht@kth.se>
SupportsDerivedPredicates no
SupportsQuantifiedPreconditions no
SupportsQuantifiedEffects no
